console.log("Yay the application is working");
var prompt = require('prompt');
var _ = require('underscore');
var fs = require('fs');
var networkArray = [];
var N = null;
var Graph = require('./Graph');
var pairLinks = []
var LD = 0;
var ND = 0;
var permanentConnections = []
var instanceSourceToDestination = [];
var delta = 0
var dPQ


var calculateMetric = function(capacity){
    var cost = 100000000/capacity
    return cost;
}
var flow = function(traffic){
    return traffic * dPQ * 12000

}

var checkIfLinkExists = function(i,j,callback){
    //function to check the link if it has been added to dictionary
    if(pairLinks.length>0){
        for(var linkInt = 0; linkInt<pairLinks.length; linkInt++){
          if(parseInt(pairLinks[linkInt].i) == parseInt(i) && parseInt(pairLinks[linkInt].j) == parseInt(j)){
                callback(linkInt);
                return;
            }
        }
        callback("nothing");
        return;
    }
    else{
        callback("nothing") ;
        return;
    }
}


var linkDelay = function(bandw, distance, traffic){
    var flowOfTraffic = traffic * dPQ * 12000 ;
    console.log('traffic:'+traffic)
    console.log("bdw:"+bandw)
    if((traffic * dPQ * 12000)>bandw){
        console.log("Taffic exceeded bandwidth");
        process.exit(code=0);
    }
    return 1/(flowOfTraffic/12000) *((flowOfTraffic/(bandw-flowOfTraffic)) + (0.001+(0.000005 * distance))*(flowOfTraffic/12000))

}

var networkDelay =  function(bandw,  distance,traffic) {
    var flowOfTraffic = traffic * dPQ * 12000
    return ((flowOfTraffic / (bandw - flowOfTraffic)) + (0.001 + (0.000005 * distance)) * (flowOfTraffic / 12000));

}


var initialize = function (){
     networkArray = [];
     N = null;
     Graph = require('./Graph');
    pairLinks = []
     LD = 0;
     ND = 0;
     permanentConnections = []
     instanceSourceToDestination = [];
     delta = 0
    dPQ = dPQ/2

fs.readFile('./assets/usa.txt','utf8',function(err,data){
    if(err){
        throw('file cannot be opened:'+err);
    }
    console.log("Internet Links are as follows");
    //console.log(data);
    var array = data.toString().split("\n");
    delta = array.length * (array.length-1) * dPQ // TODbandw,distance,tafficO - let the user be able to input this


    for(var i = 0; i<array.length; i++){
        if(i==0){
            //do nothing for now
           N = array[0]
        }
       else{ //split by array here
            var singleNodeArray = array[i].replace(/\s{2,}/g, ' ');
            //console.log(singleNodeArray)
            var singleNodeArray = singleNodeArray.split(" ");
            //console.log(singleNodeArray);
            var newObject = {};
            var newObject2 = {};
            newObject.i = singleNodeArray[1].trim();
            newObject2.i = singleNodeArray[2].trim();


            newObject.j = singleNodeArray[2].trim();
            newObject2.j = singleNodeArray[1].trim();

            newObject.c = singleNodeArray[3].trim();
            newObject2.c = singleNodeArray[3].trim();

            newObject.l = singleNodeArray[4].trim();
            newObject2.l = singleNodeArray[4].trim();


            newObject.l = newObject.l.replace('\r','');
            newObject2.l = newObject.l.replace('\r','');
            //push the object into the array
            networkArray.push(newObject);
            networkArray.push(newObject2);
        }
        }

    console.log("Data has been inputed Now Calculating Path from one node to every node in the network")
   // console.log(networkArray);
    var uniqueNodes = []
    for(var i= 0; i<networkArray.length; i++){
       // console.log(networkArray[i].i)
       //first get and index the number of uniuque nodes in the network
           var nodeId = networkArray[i].i;
        //find if the node is already in the array
       if(_.contains(uniqueNodes, networkArray[i].i)){
          //do nothing
       }
        else{
          uniqueNodes.push(networkArray[i].i)
       }
    }
    console.log("The Number of UNIQUE node IDs in the network are \n" + uniqueNodes)
    console.log('**************************');
    console.log("Now calculating the shortest distance from each node to every other node in the network....")

  // console.log(networkArray);

    //create the Graph calculating the metric for all the links
    var obj = {}
    for(var i=0;i<uniqueNodes.length;i++){
      var oneNodeWithLinks =   _.where(networkArray, {i:uniqueNodes[i]});
      var connectedTo = {}
      for(var j = 0; j < oneNodeWithLinks.length; j++){
          //calculate the cost and assign the values to the json
          var cost = 100000000/oneNodeWithLinks[j].c
          connectedTo[oneNodeWithLinks[j].j] = cost;
      }
      obj[uniqueNodes[i]] = connectedTo

    }

    console.log(obj);
    var Mesh = new Graph(obj);
    var objectPath = {}
    for(var x = 0; x<uniqueNodes.length;x++){
        objectPath[uniqueNodes[x]]= []
        //remove the value of the node from the network and set it to source
        var source = uniqueNodes[x];
        console.log('************************** \n Finding the path from '+source+' to all nodes \n *****************');
        var allPathsConnectedToSingleNode = []
        for (var z = 0; z< uniqueNodes.length; z++){
            if(source.toString()!= uniqueNodes[z].toString()){
                var singleObjLink = {}
                singleObjLink.destination = uniqueNodes[z];
                console.log('\n Destination ' + uniqueNodes[z])
                //objectPath[uniqueNodes[x]].destination = uniqueNodes[z];
                singleObjLink.path = Mesh.findShortestPath(parseInt(source), parseInt(uniqueNodes[z]));
                for(var s = 0; s<singleObjLink.path.length;s++){
                    singleObjLink.path[s] = parseInt(singleObjLink.path[s]);
                }
                console.log(singleObjLink.path);
                allPathsConnectedToSingleNode.push(singleObjLink) ;
                 //console.log(singleObjLink)
                //get the pair of links here and push it to the array if it doesnt exist
                //if it exists then add the count for that link to 1
                for(var l = 0 ; l<singleObjLink.path.length-1; l++) {
                    //console.log(pairLinks)
                    checkIfLinkExists(singleObjLink.path[l],singleObjLink.path[l+1],function(indexOfPair){
                        if(indexOfPair!="nothing"){
                            pairLinks[indexOfPair].count++;
                        }
                        else{
                            var xxx = {}
                            xxx.i = singleObjLink.path[l];
                            xxx.j = singleObjLink.path[l+1] ;
                            xxx.count = 1; //initial count number for a new pair
                            pairLinks.push(xxx);
                        }
                    });
                }
            }
        }
        objectPath[uniqueNodes[x]].path = allPathsConnectedToSingleNode;
    }

    console.log(pairLinks);
    var links = 0;
    console.log("Link Delays (ms)");
        for(x in pairLinks){
            //search for the IJ pair in the network Array
           for (var dd=0;dd<networkArray.length;dd++){
              if(networkArray[dd].i==pairLinks[x].i && networkArray[dd].j==pairLinks[x].j ){
                  console.log("For Link "+pairLinks[x].i+"->"+pairLinks[x].j )
                  var metric = calculateMetric(networkArray[dd].c);
                  console.log("Metric->"+metric);
                  var LinkDelay = linkDelay(networkArray[dd].c,networkArray[dd].l,pairLinks[x].count)  * 1000
                  console.log('LinkDelay is->'+LinkDelay)
                  LD+=LinkDelay;
                  console.log('networkdelay:'+networkDelay(networkArray[dd].c,networkArray[dd].l,pairLinks[x].count)/delta)
                  ND+= (networkDelay(networkArray[dd].c,networkArray[dd].l,pairLinks[x].count/2)*1000)/delta;
                  break;

              }
           }
            links++
        }

    console.log("Avegage Link Delay->" + (LD)/links + 'ms');
    console.log('Network Delay->'+ (ND) +'ms') ;
    main();

});
}

var schema = {
    properties: {
        DPQ: {
            pattern: /^[-+]?[0-9]*\.?[0-9]+$/,
            message: 'Only Numbers',
            required: true
        }

    }
};




//
// Get two properties from the user: username and email
//
var main = function() {
    prompt.start();
    console.log('Please Enter you DPQ \n Enter "exit" to kill the application')
    prompt.get(schema, function (err, result) {
        if(err){
            console.log(err)
        }
        dPQ = result.DPQ
        if(result.DPQ=='exit'){
            console.log("Goodbye");
            process.exit(code=0);
        }
        else{
            initialize();
        }

    });
}

main()








